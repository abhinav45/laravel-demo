After git clone you have to run below commands in your working dir[blog]:

copy .env.example to .env and change database details. 

1. composer update

2. php artisan migrate

3. php artisan passport:install

4. php artisan db:seed --class=ProductsTableSeeder

5. npm install

6. npm run dev